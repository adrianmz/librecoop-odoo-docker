FROM odoo:13

COPY odoo.conf /etc/odoo/odoo.conf

COPY requirements.txt /opt/requirements.txt

RUN pip3 install  --no-cache-dir -U pip
RUN pip3 install  --no-cache-dir -r /opt/requirements.txt
